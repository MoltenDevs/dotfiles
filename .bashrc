#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls="exa"
alias ll="exa -l"
alias la="exa -la"
alias nano="shutdown now"
eval "$(starship init bash)"
